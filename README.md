# Spring-security-demo #
Well, it's not a master piece, but it should do to demonstrate beginner level of usage of
 spring security in combination with persisted users.
 
# Steps to get this running #
1. create local mysql schema called `spring-security`
2. hit play button, to start the application
3. now you should have a table created within `spring-security` and you need to add
 an entry there that shall be used for logging in
4. open `localhost:8080/home` in browser
 
### TODO: ###
1. encrypt password
2. use different DaoAuthenticationProvider